const menuButton = document.getElementById("hamburger");
const menu = document.getElementById("containerMenu");
const menuLinks = document.querySelectorAll(".menuMobile a");

menuButton.addEventListener("click", function () {
    if (menu.style.display === "block") {
        menu.style.display = "none";
        menuButton.innerHTML = `<img src="./assets/icon-hamburger(1).svg">`;
    }
    else {
        menu.style.display = "block";
        menuButton.innerHTML = `<img src="./assets/icon-close.svg">`;
    }
})
menuLinks.forEach(link => {
    link.addEventListener("click", function () {
        menu.style.display = "none";
        menuButton.innerHTML = `<img src="./assets/icon-hamburger(1).svg">`;

    });
})